/*
  Copyright (C) 2016 D Levin (https://www.kfrlib.com)
  This file is part of KFR

  KFR is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  KFR is distributed in the hope that it will be useful,
  but WITHOUT1 ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with KFR.

  If GPL is not suitable for your project, you must purchase a commercial license to use KFR.
  Buying a commercial license is mandatory as soon as you develop commercial activities without
  disclosing the source code of your own applications.
  See https://www.kfrlib.com for details.
 */
#pragma once

namespace kfr
{
inline namespace CMT_ARCH_NAME
{

// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC complex<T1> sqrt(complex<T1> w);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC complex<T1> atanh(complex<T1> w);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC complex<T1> asin(complex<T1> w);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC T1 cmpl(T1 kx);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC complex<T1> cmpl(complex<T1> kx);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC T1 cephes_polevl(T1 x, T1 *coef, int N);

// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC void sncndn ( T1 u, T1 m, T1 &sn, T1 &cn, T1 &dn );

// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC complex<T1> inv_jacobi_sn(complex<T1> w, T1 m);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC T1 inv_jacobi_sc1(T1 w, T1 m);

// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC T1 elliptic_k (T1 m);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC T1 elliptic_km1 (T1 p);
// template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
// KFR_INTRINSIC T1 elliptic_deg ( int N, T1 m1);

} // namespace CMT_ARCH_NAME
} // namespace kfr
